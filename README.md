# Istea-InfraComoCodigo - Despliegue de Infraestructura en AWS con Terraform

Este código de Terraform crea una infraestructura básica en AWS que incluye una Virtual Private Cloud (VPC), subredes públicas y privadas, gateways de Internet y NAT, instancias EC2, grupos de seguridad, buckets S3 y políticas de acceso.


Pre-requisitos:

Tener Terraform instalado en tu sistema local.
Configurar las credenciales de AWS en tu entorno


## Uso

1. Clona este repositorio en tu máquina local:

   ```bash
   git clone <url_del_repositorio>
   cd <nombre_del_repositorio>



2. Inicializa Terraform:
   ```bash
    terraform init

3. Aplica los cambios:
   ```bash
    terraform apply


### Descripción del código
El archivo terraform.tf define los siguientes recursos:


- Proveedor AWS: Se establece la región de AWS como "us-east-1".

- VPC: Se crea una VPC con el rango de direcciones IP especificado y se le asigna una etiqueta.

- Subredes: Se crean dos subredes, una pública y otra privada, dentro de la VPC creada anteriormente. Cada subred tiene su propio rango de direcciones IP y una etiqueta.

- Internet Gateway (IGW): Se crea un IGW y se lo asocia a la VPC para permitir el tráfico hacia y desde Internet.

- NAT Gateway: Se crea un NAT Gateway y se lo asocia a la subred privada para permitir que las instancias en la subred privada accedan a Internet. Es una forma de securizar la red ya que se puede segmentar el acceso desde internet a gusto.

- IP elástica para NAT Gateway: Se asigna una dirección IP elástica al NAT Gateway.

- Tablas de enrutamiento: Se crean tablas de enrutamiento para las subredes pública y privada, y se definen las rutas para el tráfico hacia Internet a través del IGW en la subred pública y hacia Internet a través del NAT Gateway en la subred privada.

- Claves privadas TLS: Se crean claves privadas TLS para ser utilizadas en las instancias EC2.

- Grupos de seguridad: Se crean grupos de seguridad para las instancias EC2, con reglas de acceso que permiten el tráfico SSH desde cualquier dirección IP.

- Instancias EC2: Se crean instancias EC2 tanto en la subred pública como en la privada, utilizando imágenes AMI específicas, tipos de instancia, claves SSH y perfiles IAM.

- Direcciones IP elásticas: Se asocia una dirección IP elástica a la instancia EC2 en la subred pública para permitir la comunicación directa desde Internet.

- Buckets S3: Se crean dos buckets S3, uno público y otro privado, con políticas de acceso restringidas basadas en la VPC de las subredes asociadas.

- Políticas de acceso a buckets S3: Se crean políticas de acceso para los buckets S3, permitiendo el acceso desde las subredes públicas y privadas asociadas a la VPC.

- Repositorio ECR: Se crea un repositorio ECR para almacenar imágenes de contenedor Docker.

- Políticas de acceso a ECR: Se crea una política de acceso para el repositorio ECR, permitiendo acciones relacionadas con la gestión de imágenes de contenedor.

- Roles IAM y perfiles de instancia: Se crean roles IAM y perfiles de instancia para otorgar acceso a recursos específicos de AWS desde las instancias EC2.