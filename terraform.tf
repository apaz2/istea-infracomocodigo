# Define la región de AWS
provider "aws" {
  region = "us-east-1"
}

# Crea una VPC
resource "aws_vpc" "istea_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "istea_vpc"
  }
}

# Crea subredes
resource "aws_subnet" "istea_publica" {
  vpc_id            = aws_vpc.istea_vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  tags = {
    Name = "istea_subnet_publica"
  }
}

resource "aws_subnet" "istea_privada" {
  vpc_id            = aws_vpc.istea_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1a"
  tags = {
    Name = "istea_subnet_privada"
  }
}

# Crea gateways
resource "aws_internet_gateway" "istea_igw" {
  vpc_id = aws_vpc.istea_vpc.id
  tags = {
    Name = "istea_internet_gateway"
  }
}

resource "aws_nat_gateway" "istea_nat" {
  allocation_id = aws_eip.istea_eip.id
  subnet_id     = aws_subnet.istea_privada.id
  tags = {
    Name = "istea_nat_gateway"
  }
}

resource "aws_eip" "istea_eip" {
  vpc = true
  tags = {
    Name = "IpElasticaNatGateway"
  }
}

# Crea tablas de enrutamiento
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.istea_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.istea_igw.id
  }
  tags = {
    Name = "PublicRouteTable"
  }
}

resource "aws_route_table_association" "public_subnet_association" {
  subnet_id      = aws_subnet.istea_publica.id
  route_table_id = aws_route_table.public_route_table.id
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.istea_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.istea_nat.id
  }
  tags = {
    Name = "PrivateRouteTable"
  }
}

resource "aws_route_table_association" "private_subnet_association" {
  subnet_id      = aws_subnet.istea_privada.id
  route_table_id = aws_route_table.private_route_table.id
}

# Crea claves privadas
resource "tls_private_key" "istea_par_publico" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

resource "tls_private_key" "istea_par_privado" {
  algorithm = "RSA"
  rsa_bits  = 2048
}

# Crea grupos de seguridad
resource "aws_security_group" "instancia_publica_sg" {
  name        = "instancia-publica-sg"
  description = "Security Group for the public instance"
  vpc_id      = aws_vpc.istea_vpc.id
}

resource "aws_security_group_rule" "allow_ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.instancia_publica_sg.id
}

resource "aws_eip" "public_eip" {
  instance = aws_instance.instancia_publica.id
}

# Crea instancias
resource "aws_instance" "instancia_publica" {
  subnet_id                   = aws_subnet.istea_publica.id
  instance_type               = "t2.micro"
  ami                         = "ami-04e5276ebb8451442"
  key_name                    = "istea_par_publico"
  iam_instance_profile        = aws_iam_instance_profile.ec2_instance_profile.name
  security_groups             = [aws_security_group.instancia_publica_sg.id]
  associate_public_ip_address = true

  provisioner "file" {
    source      = "istea_par_publico.pem"
    destination = "/tmp/istea_par_publico.pem"
    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("istea_par_publico.pem")
      host        = self.public_ip
    }
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = file("istea_par_publico.pem")
      host        = self.public_ip
      timeout     = "5m"
    }
    inline = [
      "sudo yum update -y",
      "sudo yum install -y docker",
      "sudo service docker start",
      "sudo usermod -a -G docker ec2-user",
      "sudo service docker restart",
    ]
  }
  tags = {
    Name = "istea_instancia_publica"
  }
  depends_on = [aws_security_group_rule.allow_ssh]
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.instancia_publica.id
  allocation_id = aws_eip.public_eip.id
}

resource "aws_instance" "instancia_privada" {
  subnet_id     = aws_subnet.istea_privada.id
  instance_type = "t2.micro"
  ami           = "ami-04e5276ebb8451442"
  key_name      = "istea_par_privado"
  tags = {
    Name = "istea_instancia_privada"
  }
}

# Crea buckets S3
resource "aws_s3_bucket" "public_bucket" {
  bucket        = "istea-bucket-publico"
  acl           = "private"
  force_destroy = true
  tags = {
    Name = "isteaS3publico"
  }
}

resource "aws_s3_bucket" "private_bucket" {
  bucket        = "istea-bucket-privado"
  acl           = "private"
  force_destroy = true
  tags = {
    Name = "isteaS3privado"
  }
}

# Política para buckets
resource "aws_s3_bucket_policy" "publicBucketPolicy" {
  bucket = aws_s3_bucket.public_bucket.id
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect    = "Allow",
      Principal = "*",
      Action = [
        "s3:GetObject",
        "s3:PutObject"
      ],
      Resource = "${aws_s3_bucket.public_bucket.arn}/*",
      Condition = {
        StringEquals = {
          "aws:sourceVpc" : aws_subnet.istea_publica.vpc_id,
          "aws:sourceVpce" : "*"
        }
      }
    }]
  })
}

resource "aws_s3_bucket_policy" "privateBucketPolicy" {
  bucket = aws_s3_bucket.private_bucket.id
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect    = "Allow",
      Principal = "*",
      Action = [
        "s3:GetObject",
        "s3:PutObject"
      ],
      Resource = "${aws_s3_bucket.private_bucket.arn}/*",
      Condition = {
        StringEquals = {
          "aws:sourceVpc" : aws_subnet.istea_privada.vpc_id,
          "aws:sourceVpce" : "*"
        }
      }
    }]
  })
}

# Crea un ECR
resource "aws_ecr_repository" "my_repository" {
  name = "istea_repo"
}

# Políticas de acceso
resource "aws_security_group_rule" "allow_all_outbound" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.instancia_publica_sg.id
}

resource "aws_iam_role" "ec2_role" {
  name = "ec2-s3-access-role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect = "Allow",
      Principal = {
        Service = "ec2.amazonaws.com"
      },
      Action = "sts:AssumeRole"
    }]
  })
}

resource "aws_iam_policy" "ecr_access_policy" {
  name        = "ecr-access-policy-v2"
  description = "Policy for ECR access"
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [{
      Effect = "Allow",
      Action = [
        "ecr:GetAuthorizationToken",
        "ecr:BatchCheckLayerAvailability",
        "ecr:GetDownloadUrlForLayer",
        "ecr:BatchGetImage",
        "ecr:InitiateLayerUpload",
        "ecr:UploadLayerPart",
        "ecr:CompleteLayerUpload",
        "ecr:PutImage"
      ],
      Resource = "*"
    }]
  })
}

resource "aws_iam_role_policy_attachment" "ecr_policy_attachment" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = aws_iam_policy.ecr_access_policy.arn
}

resource "aws_iam_role_policy_attachment" "ecr_access_attachment" {
  role       = aws_iam_role.ec2_role.name
  policy_arn = aws_iam_policy.ecr_access_policy.arn
}

resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "ec2-s3-access-profile"
  role = aws_iam_role.ec2_role.name
}

resource "aws_iam_policy_attachment" "s3_access_policy_attachment" {
  name       = "s3_access_policy_attachment"
  roles      = [aws_iam_role.ec2_role.name]
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess"
}
